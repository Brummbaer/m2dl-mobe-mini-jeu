package ut3.mobe.minijeu;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class AccelerometerSensorEventListener implements SensorEventListener {
	private float deltaX = 0;
	private float deltaY = 0;

	private MainActivity activity;

	public AccelerometerSensorEventListener(SensorManager sensorManager, MainActivity mainActivity) {
		Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		this.activity = mainActivity;

		sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// get the change of the x, y, z values of the accelerometer
		deltaX = event.values[0];
		deltaY = event.values[1];

		activity.movePlayer((int) deltaX, (int) deltaY);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
}