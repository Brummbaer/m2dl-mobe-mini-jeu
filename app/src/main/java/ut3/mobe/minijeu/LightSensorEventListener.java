package ut3.mobe.minijeu;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

class LightSensorEventListener implements SensorEventListener {
   public static final int LIGHT_THRESHOLD = 20;

   float currentLux = 0;
   float maxLux;

   MainActivity activity;

   public LightSensorEventListener(SensorManager sensorManager, MainActivity mainActivity) {
      Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
      this.activity = mainActivity;

      sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
   }

   @Override
   public void onSensorChanged(SensorEvent event) {
      if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
         currentLux = event.values[0];

         Log.d("SensorActivity", String.valueOf(currentLux));

         if (currentLux < LIGHT_THRESHOLD) {
            maxLux = currentLux;

            activity.setNightMode(true);
         } else {
            activity.setNightMode(false);
         }
      }
   }

   @Override
   public void onAccuracyChanged(Sensor sensor, int i) {

   }
}
