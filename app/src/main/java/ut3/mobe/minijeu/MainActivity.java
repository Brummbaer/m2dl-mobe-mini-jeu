package ut3.mobe.minijeu;

import android.app.Activity;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends Activity {
	public static final int TIME_GAINED_ON_TARGET_REACHED = 1;

	private ImageView backgroundImage;
	private ImageView player;

	private GameView gameView;

	private TextView timerTextView;
	private int timer = 10;

	private TextView scoreTextView;
	private int score = 0;
	private GameTimer gameTimer;

	private boolean nightMode;

	private int playerX;
	private int playerY;

	private ImageView target;
	private int targetX;
	private int targetY;

	private int multiplier = 1;

	DisplayMetrics displayMetrics;

	int deviceHeight;
	int deviceWidth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		displayMetrics = new DisplayMetrics();

		setContentView(R.layout.activity_main);
		initializeInterface();

		deviceHeight = displayMetrics.heightPixels;
		deviceWidth = displayMetrics.widthPixels;

		new OnTouchEventListener(gameView, this);
		new AccelerometerSensorEventListener((SensorManager) getSystemService(SENSOR_SERVICE), this);
		new LightSensorEventListener((SensorManager) getSystemService(SENSOR_SERVICE), this);

		createNewTarget();
		gameTimer = new GameTimer(this);
	}

	private void initializeInterface() {
		backgroundImage = (ImageView) findViewById(R.id.backgroundImage);
		player = (ImageView) findViewById(R.id.player);
		scoreTextView = (TextView) findViewById(R.id.score);
		timerTextView = (TextView) findViewById(R.id.timer);
		target = findViewById(R.id.objective);
		SurfaceView gameSurface = (SurfaceView) findViewById(R.id.gameSurface);

		ViewGroup rootLayout = (ViewGroup) gameSurface.getParent();

		int gameSurfaceIndex = rootLayout.indexOfChild(gameSurface);

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		rootLayout.removeViewAt(gameSurfaceIndex);
		gameView = new GameView(this);
		rootLayout.addView(gameView, gameSurfaceIndex);

		scoreTextView.setText(getResources().getString(R.string.score) + "0");
	}

	void setNightMode(boolean nightMode) {
		if (!nightMode) {
			backgroundImage.setBackgroundResource(R.drawable.streetcurve);
		} else {
			backgroundImage.setBackgroundResource(R.drawable.streetcurve_night);
		}

		this.nightMode = nightMode;
	}

	public void createNewTarget() {
		Random random = new Random();
		targetX =
				random.nextInt(
						deviceWidth - target.getDrawable().getIntrinsicWidth()
				);

		targetY =
				random.nextInt(
						deviceHeight - target.getDrawable().getIntrinsicHeight()
				);

		target.setX(targetX);
		target.setY(targetY);
	}

	public void movePlayer(int deltaX, int deltaY) {
		/* ### X axis ### */
		if ((0 <= playerX) && (deltaX > 0)) {
			this.playerX = this.playerX - (deltaX * this.multiplier);
		}

		if ((playerX + 100 < deviceWidth) && (deltaX < 0)) {
			this.playerX = this.playerX - (deltaX * this.multiplier);
		}

		/* ### Y axis ### */
		if ((0 <= playerY) && (deltaY < 0)) {
			this.playerY = this.playerY + (deltaY * this.multiplier);
		}

		if ((playerY + 100 < deviceHeight) && (deltaY > 0)) {
			this.playerY = this.playerY + (deltaY * this.multiplier);
		}

		player.setX(playerX);
		player.setY(playerY);

		checkIfPlayerOnTarget();
	}

	private void checkIfPlayerOnTarget() {
		if (playerX > targetX - target.getWidth() && playerX < targetX + target.getWidth() &&
			playerY > targetY - target.getHeight() && playerY < targetY + target.getHeight()) {
			increaseScore();
			createNewTarget();
		}
	}

	private void increaseScore() {
		score++;
		timer += TIME_GAINED_ON_TARGET_REACHED;

		scoreTextView.setText(getResources().getString(R.string.score) + String.valueOf(this.score));
	}

	public void setMultiplier(int value) {
		this.multiplier = value;
	}

	public int getMultiplier() {
		return multiplier;
	}

	public void updateTimer(int seconds) {
		timer -= seconds;

		if (timer <= 0) {
			Toast.makeText(this, getResources().getString(R.string.game_over),
					Toast.LENGTH_SHORT).show();
		}

		timerTextView.setText(getResources().getString(R.string.timer) + String.valueOf(timer));
	}
}