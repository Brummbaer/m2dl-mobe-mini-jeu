package ut3.mobe.minijeu;

import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

class OnTouchEventListener implements View.OnTouchListener {
	private static final int MAXIMUM_ACCELERATION = 8;

	private final GameView game;
	private final MainActivity activity;

	private Handler accelerationDecay = new Handler();

	private Runnable accelerationDecayThread = new Runnable() {
		@Override
		public void run() {
			int current = activity.getMultiplier();

			if (current > 1) {
				activity.setMultiplier(current - 1);

				accelerationDecay.postDelayed(this, 1500);
			}
		}
	};

	public OnTouchEventListener(GameView gameView, MainActivity mainActivity) {
		this.game = gameView;
		this.activity = mainActivity;

		this.game.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		int x = (int) motionEvent.getX();
		int y = (int) motionEvent.getY();

		if (motionEvent.getAction() == MotionEvent.ACTION_DOWN || motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
			int current = activity.getMultiplier();

			if (current < MAXIMUM_ACCELERATION) {
				activity.setMultiplier(current + 1);
			}
		} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
			accelerationDecay.postDelayed(accelerationDecayThread, 300);
		}

		return true;
	}
}
