package ut3.mobe.minijeu;

import android.os.Handler;

class GameTimer {
	private Handler gameTimerHandler = new Handler();

	long currentTime;

	private Runnable gameTimerThread = new Runnable() {
		@Override
		public void run() {
			long t = System.currentTimeMillis();
			long millis = t - currentTime;

			int seconds = (int) (millis / 1000);
			seconds = seconds % 60;

			currentTime = t;

			activity.updateTimer(seconds);

			gameTimerHandler.postDelayed(this, 1000);
		}
	};

	private MainActivity activity;

	public GameTimer(MainActivity activity) {
		this.activity = activity;

		currentTime = System.currentTimeMillis();
		gameTimerHandler.postDelayed(gameTimerThread, 0);
	}
}
