# m2dl-mobe-mini-jeu

Blondel Richard, Lépiné Axel et Libet Guillaume

# les infos nécessaires pour lancer l'application
    - Il suffit juste de pull le code sur le lien git et lancer l'application, ou décompresser
    l'archive et l'ouvrir dans Android Studio.
    - Il faut aussi bloquer la rotation de l'écran.
  
# But du jeu 

    - Dans ce jeu nous devons déplacer un véhicule pour récuperer des orbes blanches dans un temps imparti. Chaque orbre augmente le score de 1 et augmente le temps restant au joueur. Il doit donc faire le meilleur score. 

# Fonctionnalités

    - Le joueur peut appuyer sur l'écran (onTouch) pour faire accélerer de facon progressive son véhicule. De la même manière lorsqu'il lâche le bouton il réduit sa vitesse de façon progressive. 
    - Lorsque la luminosité diminue le background est modifié, de nouveaux décors font leur apparition. 
    - Pour déplacer son véhicule le joueur doit utiliser l'accéléromètre. 
    - L'objectif une fois atteint est déplacé vers un endroit aléatoire sur l'écran. 
    - L'objectif une fois atteint augmente le temps restant au joueur et incrémente son score.

# Bug connu

Lorsque le timer arrive à 0, un message est censé s'afficher, mais celui-ci fonctionne de manière aléatoire.